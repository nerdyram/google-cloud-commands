# GCP Shell Commands

### Defining Environment Variables on GCP Shell

```
$ export PROJECT=[YOUR-PROJECT-ID]
$ export REGION=[YOUR-PREFERRED-REGION]
```

### Creating a Project using GCP Shell

```
$ gcloud projects create $PROJECT
```

### Creating a Project using GCP Shell

```
$ gcloud projects create $PROJECT
```

### Creating an App Engine Flex application in our project

If you have **NOT** a selected **REGION** yet, below command will ask you to select a region (GCP Shell will list you available regions).

```
$ gcloud app create --project=$PROJECT
```

If you determined a region, run below commands:

```
$ export REGION=[YOUR-PREFERRED-REGION]
$ gcloud app create --project=$PROJECT --region=$REGION
```

## Best Practices

### Best Practice 1: The Power of export Command

Use the power of `export` command. You can organize your work on a GCP Shell defining environment variables. Most of the commands are similar even if your project changes. (Mostly your folder paths will change) So `export` command is really handy.

```
$ export PROJECT=[YOUR-PROJECT-ID]
$ export REGION=[YOUR-PREFERRED-REGION]
$ export BILLING=[YOUR-BILLING-ID]
$ export GITHUB=[YOUR-GITHUB-PROFILE]
```

### Best Practice 2: Create a Folder for each Project

Using below command you should create a folder for easy management:

```
$ mkdir -p ${HOME}/Projects/${PROJECT}
```

Using `export` command you can easily change active working project:

```
$ export PROJECT=[ACTIVE-PROJECT-ID]
```

Your work will be easy to organize. For example using `cd` command like below is easier than using **HARDCODED** commands:

```
$ cd ${HOME}/Projects/${PROJECT}
```

### Best Practice 3: Create Foldering Standard for Quick Operations on GCP Shell

Creating foldering standard will greatly make easier your work.

For example: `${HOME}/Projects/${PROJECT}/${PACKAGE-FOLDER}` might be a good foldering structure.

### Best Practice 4: Using CI Tools for Deployments and Updates

Using Best Practice 1 and 2 you can easily define CI jobs on CI Tools like `Jenkins`.
Just run the job and you will just need to drink your coffee while the operation is progress.
